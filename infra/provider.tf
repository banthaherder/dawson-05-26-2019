variable "site_suffix" {}

provider "aws" {
  region  = "us-west-2"
  version = "~> 1.60"
}

terraform {
  backend "s3" {
    bucket = "infra.gitlab"
    key    = "terraform/state/todo-poc-site.tfstate"
    region = "us-west-2"
  }
}