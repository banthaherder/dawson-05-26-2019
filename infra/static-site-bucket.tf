module "review_app" {
    source = "./_modules/static_site"
    ref_slug = "${var.site_suffix}"
}

output "review_app_endpoint" {
    value = "${module.review_app.endpoint}"
}