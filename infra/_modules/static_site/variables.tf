variable "ref_slug" {
  type        = "string"
  description = "Suffix for s3 bucket site"
}
