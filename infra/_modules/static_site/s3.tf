resource "aws_s3_bucket" "static_site" {
  bucket = "todo-poc-${var.ref_slug}"
  acl    = "public-read"

  policy = <<EOF
{
    "Version":"2012-10-17",
    "Statement":[
        {
        "Effect":"Allow",
        "Principal": "*",
        "Action":[
            "s3:GetObject"
        ],
        "Resource":[
            "arn:aws:s3:::todo-poc-${var.ref_slug}/*"
        ]
        }
    ]
}
EOF

  website {
    index_document = "index.html"
  }

  versioning {
    enabled = false
  }

  force_destroy = true
}
