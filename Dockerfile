FROM node:12.3.0-alpine

WORKDIR /app

ADD ./package.json ./package.json

RUN npm install --silent

RUN npm install react-scripts@3.0.1 -g --silent

CMD ["npm", "run", "start"]
