# Dawson - 05/26/2019

## Installation
Local Development:
1) Ensure Docker and docker-compose is installed and running (`docker ps` and `docker-compose --version`)
2) In the top level project directory execute `docker-compose up` or `docker-compose up -d` (to run in detached mode)
3) Visit http://localhost:3000 to view the main app and http://localhost:9000 to view the coverage report.
4) Making changes will update the development applications.

GitLab:
1) Add your project repo as a remote: `git remote add $REMOTE_NAME $PROJECT_URL`
2) Push the repo to the remote: `git push $REMOTE_NAME branch/master`

## Checklist
- [x] Can run app within Docker without installing Node.js on host
- [x] Review App for app
- [x] Review App for test code coverage report

## Demo
<!-- Recommend testing these pages with Chrome Incognito to ensure we can view -->
- **Merge Request:** https://gitlab.com/banthaherder/dawson-05-26-2019/merge_requests/1
- **App Review App:** http://todo-poc-master.s3-website-us-west-2.amazonaws.com/
- **Coverage Review App:** http://todo-poc-master.s3-website-us-west-2.amazonaws.com/coverage/lcov-report/

## Features
- Terraform is incorporated into the pipeline to provide dynamic environments/resources per feature branch
- The local Dockerized development environment provides a test suite

## Security
* Addressed security concerns:
    - Least amount of privilege service user as per IAM policies.
    - AWS Access Keys are loaded into project settings as opposed to in project.

* *Not* addressed security concerns:
    - Terraform service user should be separate from deployment service user and a seperate service user should be used for the production environment.  (#6 Security Misconfigurations)
    - Public facing review apps. (#6 Security Misconfigurations, #3 Sensitive data exposure)
    - [Vulnerability scanning](https://docs.gitlab.com/ee/user/application_security/sast/). (#9 Using Components with known vulnerabilities)
    - No monitoring or logging implemented. (#10 Insufficient Logging and Monitoring)

## Improvements (future/desired)
- Database for persistent todo lists. Docker-compose db for local development environments and additional tf resources for review apps.
- Add SSL to review apps by Terraforming resources for AWS Cloudfront + AWS ACM.
- Add an authentication layer by leveraging AWS Lambda.
- Include Terraform or Ansible to create GitLab projects.

---

## Redux TodoMVC Example

This project template was built with [Create React App](https://github.com/facebookincubator/create-react-app), which provides a simple way to start React projects with no build configuration needed.

Projects built with Create-React-App include support for ES6 syntax, as well as several unofficial / not-yet-final forms of Javascript syntax such as Class Properties and JSX. See the list of [language features and polyfills supported by Create-React-App](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#supported-language-features-and-polyfills) for more information.
